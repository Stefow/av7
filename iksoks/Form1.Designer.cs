﻿namespace iksoks
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.pbDrawing00 = new System.Windows.Forms.PictureBox();
            this.pbDrawing10 = new System.Windows.Forms.PictureBox();
            this.pbDrawing01 = new System.Windows.Forms.PictureBox();
            this.pbDrawing11 = new System.Windows.Forms.PictureBox();
            this.pbDrawing20 = new System.Windows.Forms.PictureBox();
            this.pbDrawing21 = new System.Windows.Forms.PictureBox();
            this.pbDrawing02 = new System.Windows.Forms.PictureBox();
            this.pbDrawing12 = new System.Windows.Forms.PictureBox();
            this.pbDrawing22 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Igrac = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.i2 = new System.Windows.Forms.TextBox();
            this.i1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 37);
            this.button1.TabIndex = 0;
            this.button1.Text = "Započni igru";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // pbDrawing00
            // 
            this.pbDrawing00.BackColor = System.Drawing.Color.White;
            this.pbDrawing00.Enabled = false;
            this.pbDrawing00.Location = new System.Drawing.Point(27, 59);
            this.pbDrawing00.Name = "pbDrawing00";
            this.pbDrawing00.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing00.TabIndex = 1;
            this.pbDrawing00.TabStop = false;
            this.pbDrawing00.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing00_MouseClick);
            this.pbDrawing00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbDrawing00_MouseUp);
            // 
            // pbDrawing10
            // 
            this.pbDrawing10.BackColor = System.Drawing.Color.White;
            this.pbDrawing10.Enabled = false;
            this.pbDrawing10.Location = new System.Drawing.Point(27, 165);
            this.pbDrawing10.Name = "pbDrawing10";
            this.pbDrawing10.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing10.TabIndex = 2;
            this.pbDrawing10.TabStop = false;
            this.pbDrawing10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing10_MouseClick);
            this.pbDrawing10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbDrawing10_MouseUp);
            // 
            // pbDrawing01
            // 
            this.pbDrawing01.BackColor = System.Drawing.Color.White;
            this.pbDrawing01.Enabled = false;
            this.pbDrawing01.Location = new System.Drawing.Point(133, 59);
            this.pbDrawing01.Name = "pbDrawing01";
            this.pbDrawing01.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing01.TabIndex = 3;
            this.pbDrawing01.TabStop = false;
            this.pbDrawing01.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing01_MouseClick);
            this.pbDrawing01.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbDrawing01_MouseUp);
            // 
            // pbDrawing11
            // 
            this.pbDrawing11.BackColor = System.Drawing.Color.White;
            this.pbDrawing11.Enabled = false;
            this.pbDrawing11.Location = new System.Drawing.Point(133, 165);
            this.pbDrawing11.Name = "pbDrawing11";
            this.pbDrawing11.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing11.TabIndex = 4;
            this.pbDrawing11.TabStop = false;
            this.pbDrawing11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing11_MouseClick);
            this.pbDrawing11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbDrawing11_MouseUp);
            // 
            // pbDrawing20
            // 
            this.pbDrawing20.BackColor = System.Drawing.Color.White;
            this.pbDrawing20.Enabled = false;
            this.pbDrawing20.Location = new System.Drawing.Point(27, 271);
            this.pbDrawing20.Name = "pbDrawing20";
            this.pbDrawing20.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing20.TabIndex = 5;
            this.pbDrawing20.TabStop = false;
            this.pbDrawing20.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing20_MouseClick);
            this.pbDrawing20.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbDrawing20_MouseUp);
            // 
            // pbDrawing21
            // 
            this.pbDrawing21.BackColor = System.Drawing.Color.White;
            this.pbDrawing21.Enabled = false;
            this.pbDrawing21.Location = new System.Drawing.Point(133, 271);
            this.pbDrawing21.Name = "pbDrawing21";
            this.pbDrawing21.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing21.TabIndex = 6;
            this.pbDrawing21.TabStop = false;
            this.pbDrawing21.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing21_MouseClick);
            this.pbDrawing21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbDrawing21_MouseUp);
            // 
            // pbDrawing02
            // 
            this.pbDrawing02.BackColor = System.Drawing.Color.White;
            this.pbDrawing02.Enabled = false;
            this.pbDrawing02.Location = new System.Drawing.Point(239, 59);
            this.pbDrawing02.Name = "pbDrawing02";
            this.pbDrawing02.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing02.TabIndex = 7;
            this.pbDrawing02.TabStop = false;
            this.pbDrawing02.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing02_MouseClick);
            this.pbDrawing02.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbDrawing02_MouseUp);
            // 
            // pbDrawing12
            // 
            this.pbDrawing12.BackColor = System.Drawing.Color.White;
            this.pbDrawing12.Enabled = false;
            this.pbDrawing12.Location = new System.Drawing.Point(239, 165);
            this.pbDrawing12.Name = "pbDrawing12";
            this.pbDrawing12.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing12.TabIndex = 8;
            this.pbDrawing12.TabStop = false;
            this.pbDrawing12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing12_MouseClick);
            this.pbDrawing12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbDrawing12_MouseUp);
            // 
            // pbDrawing22
            // 
            this.pbDrawing22.BackColor = System.Drawing.Color.White;
            this.pbDrawing22.Enabled = false;
            this.pbDrawing22.Location = new System.Drawing.Point(239, 271);
            this.pbDrawing22.Name = "pbDrawing22";
            this.pbDrawing22.Size = new System.Drawing.Size(100, 100);
            this.pbDrawing22.TabIndex = 9;
            this.pbDrawing22.TabStop = false;
            this.pbDrawing22.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbDrawing22_MouseClick);
            this.pbDrawing22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbDrawing22_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(133, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "Igrač na potezu:";
            // 
            // Igrac
            // 
            this.Igrac.AutoSize = true;
            this.Igrac.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Igrac.ForeColor = System.Drawing.Color.Blue;
            this.Igrac.Location = new System.Drawing.Point(263, 9);
            this.Igrac.Name = "Igrac";
            this.Igrac.Size = new System.Drawing.Size(29, 20);
            this.Igrac.TabIndex = 11;
            this.Igrac.Text = "----";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Location = new System.Drawing.Point(27, 59);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(312, 312);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // i2
            // 
            this.i2.Location = new System.Drawing.Point(376, 59);
            this.i2.Name = "i2";
            this.i2.Size = new System.Drawing.Size(100, 20);
            this.i2.TabIndex = 13;
            // 
            // i1
            // 
            this.i1.Location = new System.Drawing.Point(493, 59);
            this.i1.Name = "i1";
            this.i1.Size = new System.Drawing.Size(100, 20);
            this.i1.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(372, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "Prvi igrac :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(489, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "Drugi igrac:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(416, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(478, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 20);
            this.label5.TabIndex = 18;
            this.label5.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(532, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 20);
            this.label6.TabIndex = 19;
            this.label6.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(696, 381);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.i1);
            this.Controls.Add(this.i2);
            this.Controls.Add(this.Igrac);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbDrawing22);
            this.Controls.Add(this.pbDrawing12);
            this.Controls.Add(this.pbDrawing02);
            this.Controls.Add(this.pbDrawing21);
            this.Controls.Add(this.pbDrawing20);
            this.Controls.Add(this.pbDrawing11);
            this.Controls.Add(this.pbDrawing01);
            this.Controls.Add(this.pbDrawing10);
            this.Controls.Add(this.pbDrawing00);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "IGRA XO";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pbDrawing00;
        private System.Windows.Forms.PictureBox pbDrawing10;
        private System.Windows.Forms.PictureBox pbDrawing01;
        private System.Windows.Forms.PictureBox pbDrawing11;
        private System.Windows.Forms.PictureBox pbDrawing20;
        private System.Windows.Forms.PictureBox pbDrawing21;
        private System.Windows.Forms.PictureBox pbDrawing02;
        private System.Windows.Forms.PictureBox pbDrawing12;
        private System.Windows.Forms.PictureBox pbDrawing22;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Igrac;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox i2;
        private System.Windows.Forms.TextBox i1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

