﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iksoks
{
    public partial class Form1 : Form
    {
        Graphics g, g2, g3, g4, g5, g6, g7, g8, g9;
        Pen pen1, pen2;
        int[,] polje = new int[3, 3];
        bool igraci;
        public Form1()
        {
            InitializeComponent();
            g = pbDrawing00.CreateGraphics();
            g2 = pbDrawing01.CreateGraphics();
            g3 = pbDrawing02.CreateGraphics();
            g4 = pbDrawing10.CreateGraphics();
            g5= pbDrawing11.CreateGraphics();
            g6= pbDrawing12.CreateGraphics();
            g7 = pbDrawing20.CreateGraphics();
            g8 = pbDrawing21.CreateGraphics();
            g9 = pbDrawing22.CreateGraphics();
            pen1 =new Pen(Color.Red, 6F);
            pen2 = new Pen(Color.Blue, 6F);

        }
        string igrac1="1", igrac2="2";
        private void Form1_Load(object sender, EventArgs e)
        {
            Array.Clear(polje, 0, polje.Length);
        }
        Circle c = new Circle();
        iks x = new iks();
        private void pbDrawing00_MouseUp(object sender, MouseEventArgs e)
        {
            if(igraci==true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g, pen1, e.X, e.Y);
                igraci = false;
                polje[0, 0] = 2;
            }
            else
            {
                polje[0,0] = 1;
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g, pen2, e.X, e.Y);
                igraci = true;
            }
            kraj();



        }

        private void PbDrawing00_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing00.Enabled = false;
        }

        private void PbDrawing01_MouseUp(object sender, MouseEventArgs e)
        {
            if (igraci == true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g2, pen1, e.X, e.Y);
                igraci = false;
                polje[0, 1] = 2;
            }
            else
            {
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g2, pen2, e.X, e.Y);
                igraci = true;
                polje[0, 1] = 1;
            }
            kraj();
        }

        private void PbDrawing02_MouseUp(object sender, MouseEventArgs e)
        {
            if (igraci == true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g3, pen1, e.X, e.Y);
                igraci = false;
                polje[0, 2] = 2;
            }
            else
            {
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g3, pen2, e.X, e.Y);
                igraci = true;
                polje[0, 2] = 1;
            }
            kraj();
        }

        private void PbDrawing10_MouseUp(object sender, MouseEventArgs e)
        {
            if (igraci == true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g4, pen1, e.X, e.Y);
                igraci = false;
                polje[1, 0] = 2;
            }
            else
            {
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g4, pen2, e.X, e.Y);
                igraci = true;
                polje[1, 0] = 1;
            }
            kraj();
        }

        private void PbDrawing11_MouseUp(object sender, MouseEventArgs e)
        {
            if (igraci == true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g5, pen1, e.X, e.Y);
                igraci = false;
                polje[1, 1] = 2;
            }
            else
            {
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g5, pen2, e.X, e.Y);
                igraci = true;
                polje[1, 1] = 1;
            }
            kraj();
        }

        private void PbDrawing12_MouseUp(object sender, MouseEventArgs e)
        {
            if (igraci == true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g6, pen1, e.X, e.Y);
                igraci = false;
                polje[1, 2] = 2;
            }
            else
            {
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g6, pen2, e.X, e.Y);
                igraci = true;
                polje[1, 2] = 1;
            }
            kraj();
        }

        private void PbDrawing20_MouseUp(object sender, MouseEventArgs e)
        {
            if (igraci == true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g7, pen1, e.X, e.Y);
                igraci = false;
                polje[2, 0] = 2;
            }
            else
            {
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g7, pen2, e.X, e.Y);
                igraci = true;
                polje[2, 0] = 1;
            }
            kraj();
        }

        private void PbDrawing21_MouseUp(object sender, MouseEventArgs e)
        {
            if (igraci == true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g8, pen1, e.X, e.Y);
                igraci = false;
                polje[2, 1] = 2;
            }
            else
            {
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g8, pen2, e.X, e.Y);
                igraci = true;
                polje[2, 1] = 1;
            }
            kraj();
        }
        
        private void PbDrawing22_MouseUp(object sender, MouseEventArgs e)
        {
            
            if (igraci == true)
            {
                Igrac.Text = igrac2;
                Igrac.ForeColor = Color.Blue;
                x.draw(g9, pen1, e.X, e.Y);
                igraci = false;
                polje[2, 2] = 2;
            }
            else
            {
                Igrac.Text = igrac1;
                Igrac.ForeColor = Color.Red;
                c.draw(g9, pen2, e.X, e.Y);
                igraci = true;
                polje[2, 2] = 1;
            }
            kraj();
        }
        void kraj()
        {
            int ind1 = 0, ind2 = 0;
            bool ind = true;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if ((polje[i, j] == polje[i, j + 1]) && polje[i, j] != 0)
                    {
                        ind1++;
                    }
                    if ((polje[j, i] == polje[j + 1, i]) && polje[j, i] != 0)
                    {
                        ind2++;
                    }
                }
                if (ind1 == 2 || ind2 == 2)
                {
                    button1.Enabled = true;
                    Igrac.Text = "----";
                    pobjednik();
                }
                ind1 = 0;
                ind2 = 0;
            }
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j <3; j++)
                {
                    if (polje[j, i] == 0)
                    {
                        ind = false;
                    }
                }
            }
            if (((polje[0, 0] == polje[1, 1] && polje[0, 0] == polje[2, 2]) && polje[0, 0] != 0) || (polje[2, 0] == polje[1, 1] && polje[2, 0] == polje[0, 2]) && polje[0, 2] != 0)
            {
                button1.Enabled = true;
                Igrac.Text = "----";
                pobjednik();
            }
            else
            {
                if (ind == true)
                {
                    button1.Enabled = true;
                    Igrac.Text = "----";
                    MessageBox.Show("Neriješeno", "kraj igre");
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            igrac1 = i1.Text;
            igrac2 = i2.Text;
            button1.Enabled =false;
            igraci = false;
            Igrac.Text = igrac2;
            Igrac.ForeColor = Color.Blue;
            Array.Clear(polje, 0, polje.Length);
            pbDrawing00.Enabled = true;
            pbDrawing01.Enabled = true;
            pbDrawing02.Enabled = true;
            pbDrawing10.Enabled = true;
            pbDrawing11.Enabled = true;
            pbDrawing12.Enabled = true;
            pbDrawing20.Enabled = true;
            pbDrawing21.Enabled = true;
            pbDrawing22.Enabled = true;
            i1.Enabled = false;
            i2.Enabled = false;
        }
        void pobjednik()
        {
            pbDrawing00.Enabled = false;
            pbDrawing01.Enabled = false;
            pbDrawing02.Enabled = false;
            pbDrawing10.Enabled = false;
            pbDrawing11.Enabled = false;
            pbDrawing12.Enabled = false;
            pbDrawing20.Enabled = false;
            pbDrawing21.Enabled = false;
            pbDrawing22.Enabled = false;
            if (igraci == false)
            {
                MessageBox.Show("Pobjednik je : "+igrac1, "kraj igre");
                label6.Text = (Convert.ToInt32(label6.Text) + 1).ToString();
            }
            else
            {
                MessageBox.Show("Pobjednik je : "+igrac2, "kraj igre");
                label4.Text = (Convert.ToInt32(label4.Text) + 1).ToString();
                
            }
            igraci = false;
        }
        private void PbDrawing22_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing22.Enabled = false;
        }

        private void PbDrawing21_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing21.Enabled = false;
        }

        private void PbDrawing20_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing20.Enabled = false;
        }

        private void PbDrawing12_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing12.Enabled = false;
        }

        private void PbDrawing11_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing11.Enabled = false;
        }

        private void PbDrawing10_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing10.Enabled = false;
        }

        private void PbDrawing02_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing02.Enabled = false;
        }

        private void PbDrawing01_MouseClick(object sender, MouseEventArgs e)
        {
            pbDrawing01.Enabled = false;
        }
    }
    class Circle
    {
        int r;
        public Circle()
        {
            r = 60;
        }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawEllipse(p, x-30, y-30, r, r);
        }
    }    class iks
    {
        int velicina;
        public iks()
        {
            velicina = 25;
        }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawLine(p,
                     x-1+velicina,
                     y-1+velicina,
                     x-10 - velicina,
                     y-10 - velicina);

            g.DrawLine(p,
                                 x-1 + velicina,
                                 y-10 - velicina,
                                 x-10 - velicina,
                                 y-1 + velicina);
        }
    }
}
